#include <stdio.h>

int count(int price);  
int income(int price);
int cost(int price);
int profit(int price);

int	count(int price){
	return 180 - 4 * price;
}
int income(int price){
	return count(price)*price;
}

int cost(int price){
	return count(price)*3+500;	
}

int profit(int price){
	return income(price)-cost(price);
}

int main(){
	int price;
	printf("\nExpected Profit: \n\n");
	for(price=5;price<40;price++)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",price,profit(price));
		printf("............................................................\n\n");
    } 
		return 0;	
	}
